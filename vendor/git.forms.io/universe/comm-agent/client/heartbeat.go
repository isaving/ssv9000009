//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package client

import (
	"time"

	log "git.forms.io/universe/comm-agent/common/log"
	proto "git.forms.io/universe/comm-agent/common/protocol"
)

// startHeartBeatSender is an internal program for looping up heartbeat messages.
// default session set to "default" if not specified
// default interval set to 15 if not specified
// default heartbeat collect topic set to "rapm000" if not specified
func startHeartBeatSender(beat *HeartBeat) {
	session := beat.Session
	if session == "" {
		session = "default"
	}
	interval := beat.Interval
	if interval == 0 {
		interval = 15
	}
	time.Sleep(time.Duration(interval) * time.Second)
	topic := beat.TopicId
	if topic == "" {
		topic = "rapm000"
	}
	heartBeatTopicAttribute := map[string]string{
		proto.TOPIC_TYPE:            proto.TOPIC_TYPE_HEARTBEAT,
		proto.TOPIC_ID:              topic,
		proto.TOPIC_DESTINATION_DCN: "*",
	}
	msg := UserMessage{
		SessionName:    session,
		TopicAttribute: heartBeatTopicAttribute,
	}

	// forever loop
	for {
		if err := PublishMessage(&msg); err != nil {
			log.Errorf("SendHeartbeat meet err=%v", err)
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}

// StartSendHeartBeat is use for service start a heart beat goroutine
func StartSendHeartBeat(heartBeatTopicId string, heartBeatInterval int) {
	go startHeartBeatSender(&HeartBeat{
		TopicId:  heartBeatTopicId,
		Interval: heartBeatInterval,
	})
}

// HeartBeat is a internal struct for store session name and topic id and interval
type HeartBeat struct {
	Session  string
	TopicId  string
	Interval int
}

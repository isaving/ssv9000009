//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package heartbeat

import (
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/solapp-sdk/config"
	"git.forms.io/universe/solapp-sdk/log"
)

func StartHeartbeat() {
	log.Infof("Start heartbeat with interval:%d seconds", config.CmpSvrConfig.HeartbeatIntervalSeconds)
	client.StartSendHeartBeat(config.CmpSvrConfig.HeartbeatTopicName, config.CmpSvrConfig.HeartbeatIntervalSeconds)
}

//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package event_handler

import (
	"git.forms.io/universe/comm-agent/client"
	log "git.forms.io/universe/comm-agent/common/log"
	"git.forms.io/universe/common/event_handler/constant"
	"git.forms.io/universe/common/event_handler/invoker"
	"github.com/astaxie/beego"
)

func requestHandleFunc(in client.UserMessage) (*client.UserMessage, error) {
	if userRes, err := invoker.InvokeService(in); nil != err {
		return nil, err
	} else {
		return userRes, nil
	}
}

func InitEventReceiver() error {
	log.Infof("Event handler version:%s", constant.EVENT_HANDLER_VERSION)
	client.InitSolAppClient(beego.AppConfig.DefaultInt("callback_port", 18082),
		beego.AppConfig.DefaultString("comm_agent_address", "http://127.0.0.1:18080"),
		requestHandleFunc)

	return nil
}

package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRCH0I struct {
	IntPlanNo     string ` validate:"required,max=15"`
	EffectDate    string ` validate:"required,max=10"`
	ExpDate       string
}

type DAACRCH0O struct {
	Bk            interface{} `json:"Bk"`
	EffectDate    string      `json:"EffectDate"`
	ExpDate       string      `json:"ExpDate"`
	IntCalcOption interface{} `json:"IntCalcOption"`
	IntPlanNo     string      `json:"IntPlanNo"`
	LastMaintBrno interface{} `json:"LastMaintBrno"`
	LastMaintDate interface{} `json:"LastMaintDate"`
	LastMaintTell interface{} `json:"LastMaintTell"`
	LastMaintTime interface{} `json:"LastMaintTime"`
	TccState      int         `json:"TccState"`
	ValidFlag     interface{} `json:"ValidFlag"`
}

type DAACRCH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRCH0I
}

type DAACRCH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRCH0O
}

type DAACRCH0RequestForm struct {
	Form []DAACRCH0IDataForm
}

type DAACRCH0ResponseForm struct {
	Form []DAACRCH0ODataForm
}

// @Desc Build request message
func (o *DAACRCH0RequestForm) PackRequest(DAACRCH0I DAACRCH0I) (responseBody []byte, err error) {

	requestForm := DAACRCH0RequestForm{
		Form: []DAACRCH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRCH0I",
				},
				FormData: DAACRCH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRCH0RequestForm) UnPackRequest(request []byte) (DAACRCH0I, error) {
	DAACRCH0I := DAACRCH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRCH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRCH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRCH0ResponseForm) PackResponse(DAACRCH0O DAACRCH0O) (responseBody []byte, err error) {
	responseForm := DAACRCH0ResponseForm{
		Form: []DAACRCH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRCH0O",
				},
				FormData: DAACRCH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRCH0ResponseForm) UnPackResponse(request []byte) (DAACRCH0O, error) {

	DAACRCH0O := DAACRCH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRCH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRCH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRCH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

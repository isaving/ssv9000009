package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACUTR0I struct {
	//输入是个map
}

type DAACUTR0O struct {

}

type DAACUTR0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACUTR0I
}

type DAACUTR0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACUTR0O
}

type DAACUTR0RequestForm struct {
	Form []DAACUTR0IDataForm
}

type DAACUTR0ResponseForm struct {
	Form []DAACUTR0ODataForm
}

// @Desc Build request message
func (o *DAACUTR0RequestForm) PackRequest(DAACUTR0I DAACUTR0I) (responseBody []byte, err error) {

	requestForm := DAACUTR0RequestForm{
		Form: []DAACUTR0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUTR0I",
				},
				FormData: DAACUTR0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACUTR0RequestForm) UnPackRequest(request []byte) (DAACUTR0I, error) {
	DAACUTR0I := DAACUTR0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACUTR0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUTR0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACUTR0ResponseForm) PackResponse(DAACUTR0O DAACUTR0O) (responseBody []byte, err error) {
	responseForm := DAACUTR0ResponseForm{
		Form: []DAACUTR0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACUTR0O",
				},
				FormData: DAACUTR0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACUTR0ResponseForm) UnPackResponse(request []byte) (DAACUTR0O, error) {

	DAACUTR0O := DAACUTR0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACUTR0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACUTR0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACUTR0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

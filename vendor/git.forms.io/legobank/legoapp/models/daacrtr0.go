package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRTR0I struct {
	IntRateNo	string ` validate:"required,max=20"`
	BankNo		string
	ExpFlag		string
}

type DAACRTR0O struct {
	IntRateNo     string
	BankNo        string
	Currency      string
	IntRate       float64
	EffectDate    string
	ExpFlag       string
	DayModfyFlag  string
	IntRateName   string
	Flag1         string
	Flag2         string
	Back1         float64
	Back2         float64
	Back3         string
	Back4         string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	IntRateMarket string
	IntRateUnit   string

}

type DAACRTR0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRTR0I
}

type DAACRTR0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRTR0O
}

type DAACRTR0RequestForm struct {
	Form []DAACRTR0IDataForm
}

type DAACRTR0ResponseForm struct {
	Form []DAACRTR0ODataForm
}

// @Desc Build request message
func (o *DAACRTR0RequestForm) PackRequest(DAACRTR0I DAACRTR0I) (responseBody []byte, err error) {

	requestForm := DAACRTR0RequestForm{
		Form: []DAACRTR0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTR0I",
				},
				FormData: DAACRTR0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRTR0RequestForm) UnPackRequest(request []byte) (DAACRTR0I, error) {
	DAACRTR0I := DAACRTR0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTR0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTR0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRTR0ResponseForm) PackResponse(DAACRTR0O DAACRTR0O) (responseBody []byte, err error) {
	responseForm := DAACRTR0ResponseForm{
		Form: []DAACRTR0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRTR0O",
				},
				FormData: DAACRTR0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRTR0ResponseForm) UnPackResponse(request []byte) (DAACRTR0O, error) {

	DAACRTR0O := DAACRTR0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRTR0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRTR0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRTR0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

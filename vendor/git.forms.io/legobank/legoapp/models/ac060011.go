package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060011I struct {
	AcctgAcctNo      string        `validate:"required,max=20"` //贷款核算账号
	IntRepayMod      string        `validate:"required,max=1"`  //还息模式(1-足额，2-部分还)
	TxnDate          string        `validate:"required,max=10"`
	IntRepayTotalAmt float64       `validate:"required"` //
	RecordsNum       int           `validate:"gt=0"`
	Records          []AC060011IRecords //数组
	GLOBALBIZSEQNO		string
	SRCBIZSEQNO			string
}

type AC060011IRecords struct {
	SeqNo         int     `validate:"gte=1"`
	PeriodNum     int64     `validate:"gte=0"`
	IntType       string  `validate:"required"`
	IntRepayAmt   float64 `validate:"gt=0"` //本次还息金额
	PreRepayPrins float64 `validate:"gt=0"` //提前还本金额
	RemainPrins   float64 `validate:"gt=0"`
	MgmtOrgId     string  `validate:"required"`
	AcctingOrgId  string  `validate:"required"`
	IntRepayType  string  `validate:"required"`
	Currency      string  `validate:"required"`
}

type AC060011O struct {
	AcctgAcctNo		string
	RecordsNum		int
	Records 		[]AC060011ORecord
}

type AC060011ORecord struct {
	PeriodNum           int
	IntType             string
	IntRepayAmt         float64
	UnpaidInt           float64
	RepaidInt           float64
	AccmWdAmt           float64
	AccmIntSetlAmt      float64
	UnpaidIntAmt        float64
	RepaidIntAmt        float64
	AlrdyTranOffshetInt float64
	OnshetInt           float64
	CavInt              float64
	DcValueInt          float64
	BtAmt               float64
	Status              string
}

type AC060011IDataForm struct {
	FormHead CommonFormHead
	FormData AC060011I
}

type AC060011ODataForm struct {
	FormHead CommonFormHead
	FormData AC060011O
}

type AC060011RequestForm struct {
	Form []AC060011IDataForm
}

type AC060011ResponseForm struct {
	Form []AC060011ODataForm
}

// @Desc Build request message
func (o *AC060011RequestForm) PackRequest(AC060011I AC060011I) (responseBody []byte, err error) {

	requestForm := AC060011RequestForm{
		Form: []AC060011IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060011I",
				},
				FormData: AC060011I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060011RequestForm) UnPackRequest(request []byte) (AC060011I, error) {
	AC060011I := AC060011I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060011I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060011I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060011ResponseForm) PackResponse(AC060011O AC060011O) (responseBody []byte, err error) {
	responseForm := AC060011ResponseForm{
		Form: []AC060011ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060011O",
				},
				FormData: AC060011O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060011ResponseForm) UnPackResponse(request []byte) (AC060011O, error) {

	AC060011O := AC060011O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060011O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060011O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060011I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}

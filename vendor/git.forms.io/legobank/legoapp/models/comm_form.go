package models

type CommRequestForm struct {
	Form []CommonForm
}
type CommonForm struct {
	FormHead CommonFormHead
	FormData interface{}
}

type CommonFormHead struct {
	FormId string `json:"FormId"`
}

type CommRequest struct {
	Param  map[string]string `json:"param"`
	Header map[string]string `json:"header"`
	Body   []byte            `json:"body"`
}
type CommResponse struct {
	Status int                    `json:"status"`
	Header map[string]interface{} `json:"header"`
	Body   []byte                 `json:"body"`
}
type PermanentApi struct {
	NodeId    string
	Signature string
	Nonce     string
	Timestamp int64
}
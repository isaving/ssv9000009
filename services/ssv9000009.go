//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv9000009/constant"
	"git.forms.io/isaving/sv/ssv9000009/dao"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
	"time"
)

var Ssv9000009Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv9000009",
	ConfirmMethod: "ConfirmSsv9000009",
	CancelMethod:  "CancelSsv9000009",
}

type Ssv9000009 interface {
	TrySsv9000009(*models.SSV9000009I) (*models.SSV9000009O, error)
	ConfirmSsv9000009(*models.SSV9000009I) (*models.SSV9000009O, error)
	CancelSsv9000009(*models.SSV9000009I) (*models.SSV9000009O, error)
}

type Ssv9000009Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv900009O *models.SSV9000009O
	Ssv900009I *models.SSV9000009I
	O          *xorm.Engine
}

// @Desc Ssv9000009 process
// @Author
// @Date 2020-12-12
func (impl *Ssv9000009Impl) TrySsv9000009(ssv9000009I *models.SSV9000009I) (ssv9000009O *models.SSV9000009O, err error) {

	//TODO Service Business Process,No Need Validate Paramer,Paramer Has Already Validate in Controller
	impl.Ssv900009I = ssv9000009I
	impl.O = dao.EngineCache

	QueryTevsbmxpRecord, err := dao.QueryTevsbmxpById(impl.Ssv900009I.GlobalBizSeqNo, impl.O)
	if err != nil {
		log.Info("QueryTevsbmxpById failed")
		return nil, err
	}
	if QueryTevsbmxpRecord != nil {
		return nil, errors.New("Query Duplicate primary key！", constants.ERRCODE4)
	}

	Tevsbmxp := &dao.T_evsbmxp{
		TransationDate:          impl.Ssv900009I.TransationDate,
		TransationTime:          time.Now().Format("2006-01-02 15:04:05"),
		GlobalBizSeqNo:          impl.Ssv900009I.GlobalBizSeqNo,
		SrcBizseqNo:             impl.Ssv900009I.SrcBizseqNo,
		SrcTimeStamp:            impl.Ssv900009I.SrcTimeStamp,
		SrcSysId:                impl.Ssv900009I.SrcSysId,
		SrcDcn:                  impl.Ssv900009I.SrcDcn,
		OrgChannelType:          impl.Ssv900009I.OrgChannelType,
		TxDeviceId:              impl.Ssv900009I.TxDeviceId,
		TxDeptCode:              impl.Ssv900009I.TxDeptCode,
		BnAcc:                   impl.Ssv900009I.BnAcc,
		TransationEm:            impl.Ssv900009I.TransationEm,
		TransationType:          impl.Ssv900009I.TransationType,
		TransationAmt:           impl.Ssv900009I.TransationAmt,
		PairNm:                  impl.Ssv900009I.PairNm,
		BatchNm:                 impl.Ssv900009I.BatchNm,
		TradeFlag:               impl.Ssv900009I.TradeFlag,
		ReverseTradeFlag:        impl.Ssv900009I.ReverseTradeFlag,
		OriginalTransactionDate: impl.Ssv900009I.OriginalTransactionDate,
		OriginalGlobalBizseqNo:  impl.Ssv900009I.OriginalGlobalBizseqNo,
		OriginalSrcBizseqNo:     impl.Ssv900009I.OriginalSrcBizseqNo,
		TranResult:              impl.Ssv900009I.TranResult,
		ErrorCode:               impl.Ssv900009I.ErrorCode,
		LastUpDatetime:          time.Now().Format("2006-01-02 15:04:05"),
		LastUpBn:                impl.SrcAppProps[constant.TXDEPTCODE],
		LastUpEm:                impl.SrcAppProps[constant.TXUSERID],
	}
	if err := dao.InsertTevsbmxpOne(Tevsbmxp, impl.O); err != nil {
		log.Info("Insert tevsbmxp record to database failed！")
		return nil, err
	}

	ssv9000009O = &models.SSV9000009O{
		//TODO Assign  value to the OUTPUT Struct field
	}

	return ssv9000009O, nil
}

func (impl *Ssv9000009Impl) ConfirmSsv9000009(ssv9000009I *models.SSV9000009I) (ssv9000009O *models.SSV9000009O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv9000009")
	return nil, nil
}

func (impl *Ssv9000009Impl) CancelSsv9000009(ssv9000009I *models.SSV9000009I) (ssv9000009O *models.SSV9000009O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv9000009")
/*	impl.O = dao.EngineCache
	if err := dao.DeleteTevsbmxpOne(impl.SrcAppProps[constant.GLOBALBIZSEQNO], impl.O); err != nil {
		log.Info("delete t_evsbmxp failed！")
		return nil, err
	}*/

	return nil, nil
}

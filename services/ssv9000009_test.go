//Version: 1.0.0
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000009/dao"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	_ = beego.LoadAppConfig("ini", "../conf/app.conf")
	_ = dao.InitDatabase()
	exitVal := m.Run()
	os.Exit(exitVal)
}


func TestSsv9000009Impl_Ssv9000009_Success_Qurey(t *testing.T) {

	input := &models.SSV9000009I{
		TransationDate: "11",
		TransationTime: "11",
		GlobalBizSeqNo: "111",
	}

	impl := Ssv9000009Impl{
		Ssv900009I: input,
		O:          dao.EngineCache,
	}
	maps := make(map[string]string)
	maps["GlobalBizSeqNo"] = "10013"
	//impl.SrcAppProps = maps
	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectQuery("t_evsbmxp").
				WillReturnRows(sqlmock.NewRows([]string{"global_biz_seqno"}))
			//mock.ExpectExec("t_evsbmxp").WithArgs(input).WillReturnResult(sqlmock.NewResult(50, 50))
			Convey("Validate session", func() {
				_, _ = impl.TrySsv9000009(input)
				_, _ = impl.ConfirmSsv9000009(input)
				_, _ = impl.CancelSsv9000009(input)
			})
		})

	})
}

func TestSsv9000009Impl_Ssv9000009_Success(t *testing.T) {

	input := &models.SSV9000009I{
		TransationDate: "11",
		TransationTime: "11",
		GlobalBizSeqNo: "111",
	}

	impl := Ssv9000009Impl{
		Ssv900009I: input,
		O:          dao.EngineCache,
	}
	maps := make(map[string]string)
	maps["GlobalBizSeqNo"] = "10013"
	impl.SrcAppProps = maps
	Convey("Call service", t, func() {
		//var session *dao.Session
		Convey("Mock data", func() {
			db, mock, err := sqlmock.New()
			So(err, ShouldBeNil)
			dao.EngineCache.DB().DB = db
			mock.ExpectExec("t_evsbmxp").WithArgs(input).WillReturnResult(sqlmock.NewResult(50, 50))
			Convey("Validate session", func() {
				_, _ = impl.TrySsv9000009(input)
				_, _ = impl.ConfirmSsv9000009(input)
				_, _ = impl.CancelSsv9000009(input)
			})
		})

	})
}


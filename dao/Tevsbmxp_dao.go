package dao

import (
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/xormplus/xorm"
)

type T_evsbmxp struct {
	TransationDate          string  `xorm:"'transation_date'"`           //交易日期
	TransationTime          string  `xorm:"'transation_time'"`           //交易时间
	GlobalBizSeqNo          string  `xorm:"'global_biz_seqno' pk"`       //全局流水号
	SrcBizseqNo             string  `xorm:"'src_biz_seq_no'"`            //业务流水号
	SrcTimeStamp            string  `xorm:"'src_time_stamp'"`            //源系统工作日期时间
	SrcSysId                string  `xorm:"'src_sysid'"`                 //源系统编码
	SrcDcn                  string  `xorm:"'src_dcn'"`                   //源系统DCN
	OrgChannelType          string  `xorm:"'org_channel_type'"`          //交易发起渠道
	TxDeviceId              string  `xorm:"'tx_device_id'"`              //终端号
	TxDeptCode              string  `xorm:"'tx_dept_code'"`              //交易行所
	BnAcc                   string  `xorm:"'bn_acc'"`                    //账务行所
	TransationEm            string  `xorm:"'transation_em'"`             //交易柜员
	TransationType          string  `xorm:"'transation_type'"`           //交易方式
	TransationAmt           float64 `xorm:"'transation_amt'"`            //交易金额
	PairNm                  string  `xorm:"'pair_nm'"`                   //套号
	BatchNm                 string  `xorm:"'batch_nm'"`                  //批号
	TradeFlag               string  `xorm:"'trade_flag'"`                //反交易标志
	ReverseTradeFlag        string  `xorm:"'reverse_trade_flag'"`        //冲正交易标志
	OriginalTransactionDate string  `xorm:"'original_transaction_date'"` //原交易日期
	OriginalGlobalBizseqNo  string  `xorm:"'original_global_bizseq_no'"` //原全局流水号
	OriginalSrcBizseqNo     string  `xorm:"'original_src_bizseq_no'"`    //原业务流水号
	TranResult              string  `xorm:"'tran_result'"`               //处理状态
	ErrorCode               string  `xorm:"'error_code'"`                //错误码
	ErrorDesc               string  `xorm:"'error_desc'"`                //错误描述
	LastUpDatetime          string  `xorm:"'last_up_datetime'"`          //最后更新日期时间
	LastUpBn                string  `xorm:"'last_up_bn'"`                //最后更新行所
	LastUpEm                string  `xorm:"'last_up_em'"`                //最后更新柜员
}

func (t *T_evsbmxp) TableName() string {
	return "t_evsbmxp"
}

func InsertTevsbmxpOne(tevsbmxp *T_evsbmxp, o *xorm.Engine) error {
	num, err := o.Insert(tevsbmxp)
	if err != nil {
		return err
	}
	if num != 1 {
		return errors.New("Insert tevsbmxp record to database failed", "")
	}
	return nil
}

func DeleteTevsbmxpOne(GlobalBizseqNo string, o *xorm.Engine) error {
	sql := "delete from t_evsbmxp where global_biz_seqno =?"
	_, err := o.Exec(sql, GlobalBizseqNo)
	if err != nil {
		log.Info("delete t_evsbmxp failed")
		return err
	}
	return nil
}

func QueryTevsbmxpById(GlobalBizseqNo string, o *xorm.Engine) (t_evsbmxp *T_evsbmxp, err error) {
	t_evsbmxp = &T_evsbmxp{
		GlobalBizSeqNo: GlobalBizseqNo,
	}
	log.Debug("Query where：", t_evsbmxp)
	exists, err := o.Get(t_evsbmxp)
	if err != nil {
		log.Info("The product information does not exist")
		return nil, err
	}
	if !exists {
		log.Info("Record is empty")
		return nil, nil
	}
	return t_evsbmxp, nil
}

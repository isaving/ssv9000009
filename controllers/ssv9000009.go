//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv9000009/dao"
	"git.forms.io/isaving/sv/ssv9000009/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv9000009Controller struct {
	controllers.CommTCCController
}

func (*Ssv9000009Controller) ControllerName() string {
	return "Ssv9000009Controller"
}

// @Desc ssv9000009 controller
// @Description Entry
// @Param ssv9000009 body models.SSV9000009I true "body for user content"
// @Success 200 {object} models.SSV9000009O
// @router /ssv9000009 [post]
// @Author
// @Date 2020-12-12
func (c *Ssv9000009Controller) Ssv9000009() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv9000009Controller.Ssv9000009 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv9000009I := &models.SSV9000009I{}
	if err := models.UnPackRequest(c.Req.Body, ssv9000009I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv9000009I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv9000009 := &services.Ssv9000009Impl{}
	ssv9000009.New(c.CommTCCController)
	ssv9000009.Ssv900009I = ssv9000009I
	ssv9000009.O = dao.EngineCache
	ssv9000009Compensable := services.Ssv9000009Compensable

	proxy, err := aspect.NewDTSProxy(ssv9000009, ssv9000009Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv9000009I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv9000009O, ok := rsp.(*models.SSV9000009O); ok {
		if responseBody, err := models.PackResponse(ssv9000009O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv9000009 Controller
// @Description ssv9000009 controller
// @Param Ssv9000009 body models.SSV9000009I true body for SSV9000009 content
// @Success 200 {object} models.SSV9000009O
// @router /create [post]
/*func (c *Ssv9000009Controller) SWSsv9000009() {
	//Here is to generate API documentation, no need to implement methods
}
*/